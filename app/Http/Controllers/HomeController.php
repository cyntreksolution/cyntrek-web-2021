<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function services(){
        return view('services');
    }

    public function portfolio(){
        return view('portfolio');
    }

    public function aboutUs(){
        return view('about');
    }

    public function contactUs(){
        return view('contact');
    }

    public function blog(){
        return view('blog');
    }
}
