<header id="header" class="header-effect-shrink"
        data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-top-0">
        <div class="header-container container-fluid px-lg-4">
            <div class="header-row">
                <div class="header-column header-column-border-right flex-grow-0">
                    <div class="header-row pr-4">
                        <div class="header-logo">
                            <a href="/">
                                <img alt="Porto" width="75" height="auto" data-sticky-width="62"
                                     data-sticky-height="auto" src="{{asset('img/cyntrek-logo.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav header-nav-links justify-content-right pr-4">
                            <div
                                class="header-nav-main header-nav-main-square header-nav-main-font-lg-upper header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse header-mobile-border-top">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle active" href="{{route('home')}}"> Home </a>
                                        </li>
                                        <li class="dropdown dropdown-mega">
                                            <a class="dropdown-item dropdown-toggle" href="{{route('services')}}"> Services </a>
                                        </li>
{{--                                        <li class="dropdown">--}}
{{--                                            <a class="dropdown-item dropdown-toggle" href="{{route('portfolio')}}">Portfolio </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="dropdown">--}}
{{--                                            <a class="dropdown-item dropdown-toggle" href="{{route('blog')}}"> Blog </a>--}}
{{--                                        </li>--}}
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{route('about')}}">About Us </a>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{route('contact')}}">Contact Us </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-column header-column-border-left flex-grow-0 justify-content-center">
                    <div class="header-row pl-5 justify-content-end">
                        <div class="header-nav-features-search-reveal-container">
                            <div
                                class="header-nav-feature header-nav-features-search header-nav-features-search-reveal d-inline-flex">
                                <a href="#"
                                   class="header-nav-features-search-show-icon d-inline-flex text-decoration-none"><i
                                        class="fas fa-search header-nav-top-icon"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-nav-features header-nav-features-no-border p-static z-index-2">
                    <button class="btn header-btn-collapse-nav ml-0 ml-sm-3" data-toggle="collapse"
                            data-target=".header-nav-main nav">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div
                        class="header-nav-feature header-nav-features-search header-nav-features-search-reveal header-nav-features-search-reveal-big-search px-3">
                        <form role="search" class="d-flex w-100 h-100"
                              action="https://www.okler.net/previews/porto/8.3.0/page-search-results.html"
                              method="get">
                            <div class="big-search-header input-group">
                                <input class="form-control text-1" id="headerSearch" name="q" type="search" value=""
                                       placeholder="Type and hit enter...">
                                <a href="#" class="header-nav-features-search-hide-icon"><i
                                        class="fas fa-times header-nav-top-icon"></i></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
