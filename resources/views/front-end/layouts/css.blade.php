<link id="googleFonts"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&amp;display=swap"
      rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/animate/animate.compat.css')}}">
<link rel="stylesheet" href="{{asset('vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendor/rs-plugin/revolution-addons/typewriter/css/typewriter.css')}}"/>
<link rel="stylesheet" href="{{asset('css/theme.css')}}">
<link rel="stylesheet" href="{{asset('css/theme-elements.css')}}">
<link rel="stylesheet" href="{{asset('css/theme-blog.css')}}">
<link rel="stylesheet" href="{{asset('css/theme-shop.css')}}">
<link id="skinCSS" rel="stylesheet" href="{{asset('css/skins/skin-corporate-15.css')}}">

<link rel="stylesheet" href="{{asset('css/custom.css')}}">

<script src="{{asset('vendor/modernizr/modernizr.min.js')}}"></script>
