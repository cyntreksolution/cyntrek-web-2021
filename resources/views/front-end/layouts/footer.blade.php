<footer id="footer" class="mt-0 bg-color-light-scale-1 border-0">
    <div class="container">
        <div class="row py-5 justify-content-center">
            <div class="col-md-9 offset-md-1 offset-lg-0 mb-4 mb-lg-0 d-flex align-items-center">
                <div class="footer-nav footer-nav-links footer-nav-bottom-line">
                    <nav>
                        <ul class="nav" id="footerNav">
                            <li>
                                <a href="{{route('home')}}" class="text-color-dark active">Home</a>
                            </li>
                            <li>
                                <a href="{{route('services')}}" class="text-color-dark">Services</a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="{{route('portfolio')}}" class="text-color-dark">Portfolio</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="{{route('blog')}}" class="text-color-dark">Blog</a>--}}
{{--                            </li>--}}
                            <li>
                                <a href="{{route('about')}}" class="text-color-dark">About Us</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}" class="text-color-dark">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 mb-4 mb-lg-0 text-center text-lg-right">
                <h5 class="text-3 text-color-dark mb-0 pb-1 opacity-6">CONTACT US</h5>
                <a href="tel:+1234567890"
                   class="text-8 text-color-dark text-color-hover-primary text-decoration-none font-weight-bold pb-1 mb-0">(800)
                    123-4567</a>
                <p class="m-0"><a
                        href="https://www.okler.net/cdn-cgi/l/email-protection#a6cbc7cfcae6c3dec7cbd6cac388c5c9cb"
                        class="text-color-dark text-color-hover-primary"><span class="__cf_email__"
                                                                               data-cfemail="9df0fcf4f1ddf8e5fcf0edf1f8b3fef2f0">[email&#160;protected]</span></a>
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright footer-copyright-style-2 bg-color-light-scale-1 footer-top-border">
        <div class="container py-2">
            <div class="row py-4">
                <div
                    class="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                    <a href="{{route('home')}}" class="logo pr-0 pr-lg-3">
                        <img alt="Porto Website Template" src="{{asset('img/cyntrek-logo.png')}}" height="64">
                    </a>
                </div>
                <div
                    class="col-lg-11 d-flex align-items-center justify-content-center justify-content-lg-end mb-4 mb-lg-0">
                    <p>© Copyright {{date('Y')}}. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
