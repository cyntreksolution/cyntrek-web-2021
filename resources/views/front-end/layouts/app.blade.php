<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cyntrek Solutions</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="{{asset('img/favicon.jpg')}}" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    @include('front-end.layouts.css')

</head>
<body data-plugin-page-transition>

<div class="body">
    @include('front-end.layouts.header')

    @yield('content')

    @include('front-end.layouts.footer')
</div>


@include('front-end.layouts.js')

</body>

</html>
