@extends('front-end.layouts.app')

@section('content')
    <div role="main" class="main">
        <div
            class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual bg-color-light-scale-1 dots-inside dots-horizontal-center show-dots-hover show-dots-xs nav-inside nav-inside-plus nav-light nav-lg nav-font-size-md show-nav-hover rounded-nav mb-0"
            data-plugin-options="{'autoplayTimeout': 3000}" style="height: calc( 100vh - 100px );">
            <div class="owl-stage-outer">
                <div class="owl-stage">
                    <div class="owl-item position-relative" style="height: calc( 100vh - 100px );">
                        <div class="container position-relative z-index-3 h-100">
                            <div class="row align-items-center h-100">
                                <div class="col-md-9 col-lg-6">
                                    <h1 class="text-color-dark font-weight-extra-bold text-12 line-height-1 mb-3 appear-animation"
                                        data-appear-animation="fadeInLeftShorterPlus" data-appear-animation-delay="500"
                                        data-appear-animation-duration="500ms"
                                        data-plugin-options="{'minWindowWidth': 0}">Hey, welcome to our amazing
                                        office.</h1>
                                    <p class="text-4-5 text-color-dark font-weight-light mb-4"
                                       data-plugin-animated-letters
                                       data-plugin-options="{'startDelay': 600, 'minWindowWidth': 0, 'animationSpeed': 50}">
                                        Porto is a huge success, learn more about us.</p>
                                    <a href="#"
                                       class="btn btn-primary btn-modern font-weight-bold text-3 py-3 btn-px-5 mt-2 appear-animation"
                                       data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000"
                                       data-plugin-options="{'minWindowWidth': 0}">GET STARTED NOW <i
                                            class="fas fa-arrow-right ml-2"></i></a>
                                </div>
                                <div class="col-lg-5 offset-lg-1 d-none d-lg-block">
                                    <div class="position-relative appear-animation"
                                         data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1500"
                                         data-plugin-options="{'minWindowWidth': 0}">
                                        <img src="img/slides/slide-corporate-15-1.png" class="img-fluid" alt=""/>
                                        <div class="position-absolute top-50pct left-50pct transform3dxy-n50">
                                            <img src="img/slides/circle.png" class="appear-animation" alt=""
                                                 data-appear-animation="fadeIn" data-appear-animation-delay="1700"
                                                 data-appear-animation-duration="4s"
                                                 data-plugin-options="{'minWindowWidth': 0}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="owl-item position-relative" style="height: calc( 100vh - 100px );">
                        <div class="container position-relative z-index-3 h-100">
                            <div class="row align-items-center h-100">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="position-relative appear-animation"
                                         data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000"
                                         data-plugin-options="{'minWindowWidth': 0}">
                                        <img src="img/slides/slide-corporate-15-2.png" class="img-fluid" alt=""/>
                                        <div class="position-absolute top-50pct left-50pct transform3dxy-n50">
                                            <img src="img/slides/circle.png" class="appear-animation" alt=""
                                                 data-appear-animation="fadeIn" data-appear-animation-delay="1300"
                                                 data-appear-animation-duration="4s"
                                                 data-plugin-options="{'minWindowWidth': 0}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-6 offset-lg-1">
                                    <h1 class="text-color-dark font-weight-extra-bold text-12 line-height-1 mb-3 appear-animation"
                                        data-appear-animation="fadeInLeftShorterPlus" data-appear-animation-delay="500"
                                        data-appear-animation-duration="500ms"
                                        data-plugin-options="{'minWindowWidth': 0}">We can help you with your
                                        project.</h1>
                                    <p class="text-4-5 text-color-dark font-weight-light mb-4"
                                       data-plugin-animated-letters
                                       data-plugin-options="{'startDelay': 600, 'minWindowWidth': 0, 'animationSpeed': 50}">
                                        Porto is a huge success, learn more about us.</p>
                                    <a href="#"
                                       class="btn btn-primary btn-modern font-weight-bold text-3 py-3 btn-px-5 mt-2 appear-animation"
                                       data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000"
                                       data-plugin-options="{'minWindowWidth': 0}">GET STARTED NOW <i
                                            class="fas fa-arrow-right ml-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-nav">
                <button type="button" role="presentation" class="owl-prev"></button>
                <button type="button" role="presentation" class="owl-next"></button>
            </div>
        </div>

        <div class="container container-lg-custom py-5 my-5">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center mb-4">
                    <h2 class="font-weight-bold text-8 mb-3 appear-animation" data-appear-animation="fadeIn">Explore All
                        Possibilities</h2>
                    <p class="line-height-9 text-4 appear-animation" data-appear-animation="fadeInUpShorter"
                       data-appear-animation-delay="200"><span class="opacity-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat id sapien ac varius. Fusce hendrerit ligula a consectetur ullamcorper. Vestibulum varius pharetra lorem, in maximus libero placerat sed.</span>
                    </p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10 px-lg-5">
                    <div class="row">
                        <div class="col-md-6 mb-2 pb-2 px-2 appear-animation"
                             data-appear-animation="fadeInRightShorter">
<span
    class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info thumb-info-no-zoom thumb-info-slide-info-hover">
<span class="thumb-info-wrapper thumb-info-wrapper-no-opacity">
<img src="img/office/our-office-9.jpg" class="img-fluid" alt="">
<span class="thumb-info-title">
<span class="thumb-info-slide-info-hover-1">
<span class="thumb-info-inner text-4">Our Services</span>
</span>
<span class="thumb-info-slide-info-hover-2">
<span class="thumb-info-inner text-2">
<a href="#"
   class="d-inline-flex align-items-center btn btn-light text-color-dark font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW OUR SERVICES <i
        class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
</span>
</span>
</span>
</span>
</span>
                        </div>
                        <div class="col-md-6 mb-2 pb-2 px-2 appear-animation" data-appear-animation="fadeInLeftShorter">
<span
    class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info thumb-info-no-zoom thumb-info-slide-info-hover">
<span class="thumb-info-wrapper thumb-info-wrapper-no-opacity">
<img src="img/office/our-office-10.jpg" class="img-fluid" alt="">
<span class="thumb-info-title">
<span class="thumb-info-slide-info-hover-1">
<span class="thumb-info-inner text-4">Our Team</span>
</span>
<span class="thumb-info-slide-info-hover-2">
<span class="thumb-info-inner text-2">
<a href="#"
   class="d-inline-flex align-items-center btn btn-light text-color-dark font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW OUR TEAM <i
        class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
</span>
</span>
</span>
</span>
</span>
                        </div>
                        <div class="col-md-4 mb-2 pb-2 px-2 appear-animation" data-appear-animation="fadeInLeftShorter">
<span
    class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info thumb-info-no-zoom thumb-info-slide-info-hover">
<span class="thumb-info-wrapper thumb-info-wrapper-no-opacity">
<img src="img/office/our-office-11.jpg" class="img-fluid" alt="">
<span class="thumb-info-title">
<span class="thumb-info-slide-info-hover-1">
<span class="thumb-info-inner text-4">Our Offices</span>
</span>
<span class="thumb-info-slide-info-hover-2">
<span class="thumb-info-inner text-2">
<a href="#"
   class="d-inline-flex align-items-center btn btn-light text-color-dark font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW OUR OFFICES <i
        class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
</span>
</span>
</span>
</span>
</span>
                        </div>
                        <div class="col-md-4 mb-2 pb-2 px-2 appear-animation" data-appear-animation="fadeInRightShorter"
                             data-appear-animation-delay="200">
<span
    class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info thumb-info-no-zoom thumb-info-slide-info-hover">
<span class="thumb-info-wrapper thumb-info-wrapper-no-opacity">
<img src="img/office/our-office-12.jpg" class="img-fluid" alt="">
<span class="thumb-info-title">
<span class="thumb-info-slide-info-hover-1">
<span class="thumb-info-inner text-4">Our Mission</span>
</span>
<span class="thumb-info-slide-info-hover-2">
<span class="thumb-info-inner text-2">
<a href="#"
   class="d-inline-flex align-items-center btn btn-light text-color-dark font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW OUR MISSION <i
        class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
</span>
</span>
</span>
</span>
</span>
                        </div>
                        <div class="col-md-4 px-2 appear-animation" data-appear-animation="fadeInUpShorter"
                             data-appear-animation-delay="400">
<span
    class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-info thumb-info-no-zoom thumb-info-slide-info-hover">
<span class="thumb-info-wrapper thumb-info-wrapper-no-opacity">
<img src="img/office/our-office-13.jpg" class="img-fluid" alt="">
<span class="thumb-info-title">
<span class="thumb-info-slide-info-hover-1">
<span class="thumb-info-inner text-4">Our Contact</span>
</span>
<span class="thumb-info-slide-info-hover-2">
<span class="thumb-info-inner text-2">
<a href="#"
   class="d-inline-flex align-items-center btn btn-light text-color-dark font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW OUR CONTACT <i
        class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
</span>
</span>
</span>
</span>
</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section section-no-border section-height-4 mb-0">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-4 appear-animation" data-appear-animation="fadeInLeftBig">
                        <h4 class="font-weight-bold mb-2 mt-4 mt-md-0">Mobile Apps</h4>
                        <p class="px-lg-4 mb-0">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim.
                            Nullam id varius.
                        </p>
                    </div>
                    <div class="col-md-4 mb-2 appear-animation" data-appear-animation="fadeIn">
                        <h4 class="font-weight-bold mb-2 mt-4 mt-md-0">Creative Websites</h4>
                        <p class="px-lg-4 mb-0">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim.
                            Nullam id varius.
                        </p>
                    </div>
                    <div class="col-md-4 appear-animation" data-appear-animation="fadeInRightBig">
                        <h4 class="font-weight-bold mb-2 mt-4 mt-md-0">SEO Optimization</h4>
                        <p class="px-lg-4 mb-0">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim.
                            Nullam id varius.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-no-border section-height-3 bg-color-primary my-0">
            <div class="container">
                <div class="row counters counters-sm text-light">
                    <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation"
                         data-appear-animation="fadeInLeftShorter">
                        <div class="counter">
                            <strong data-to="18000" data-append="+">0</strong>
                            <label class="text-light opacity-6 font-weight-normal pt-1">Happy Clients</label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation"
                         data-appear-animation="fadeInDownShorter">
                        <div class="counter">
                            <strong data-to="3500" data-append="+">0</strong>
                            <label class="text-light opacity-6 font-weight-normal pt-1">Answered Tickets</label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0 appear-animation"
                         data-appear-animation="fadeInUpShorter">
                        <div class="counter">
                            <strong data-to="16">0</strong>
                            <label class="text-light opacity-6 font-weight-normal pt-1">Pre-made Demos</label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 appear-animation" data-appear-animation="fadeInRightShorter">
                        <div class="counter">
                            <strong data-to="3000" data-append="+">0</strong>
                            <label class="text-light opacity-6 font-weight-normal pt-1">Development Hours</label>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container my-5">
            <div class="row py-5">
                <div class="col">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="featured-boxes featured-boxes-modern-style-1">
                                <div class="featured-box overlay overlay-show overlay-op-9 border-radius border-0">
                                    <div class="featured-box-background"
                                         style="background-image: url({{asset('img/gallery/gallery-11.jpg')}}); background-size: cover; background-position: center;"></div>
                                    <div class="box-content px-lg-4 px-xl-5 py-lg-5">
                                        <div class="py-5 my-4">
                                            <a class="text-decoration-none lightbox" href="https://vimeo.com/45830194"
                                               data-plugin-options="{'type':'iframe'}">
                                                <img class="icon-animated" width="60"
                                                     src="vendor/linea-icons/linea-music/icons/play-button.svg" alt=""
                                                     data-icon
                                                     data-plugin-options="{'color': '#FFF', 'animated': true, 'delay': 600, 'strokeBased': true}"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 text-center text-lg-left">
                            <h4 class="text-6 font-weight-bold line-height-5 mt-4 mt-lg-0">We are sure you will enjoy
                                work with us and our team.</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat id sapien ac
                                varius. Fusce hendrerit ligula a con.</p>
                            <a href="#"
                               class="d-inline-flex align-items-center btn btn-dark text-color-light font-weight-bold px-4 btn-py-2 text-1 rounded">VIEW
                                OUR SERVICES <i class="fa fa-arrow-right ml-2 pl-1 text-3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

