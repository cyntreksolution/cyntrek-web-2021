<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index'])->name('home');
Route::get('/services',[HomeController::class,'services'])->name('services');
Route::get('/portfolio',[HomeController::class,'portfolio'])->name('portfolio');
Route::get('/about-us',[HomeController::class,'aboutUs'])->name('about');
Route::get('/contact-us',[HomeController::class,'contactUs'])->name('contact');
Route::get('/blog',[HomeController::class,'blog'])->name('blog');
